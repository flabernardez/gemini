<?php

if ( ! function_exists( 'taurus_theme_setup' ) ) :

	function gemini_theme_setup() {

		load_theme_textdomain( 'gemini-theme', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_post_type_support( 'page', 'excerpt' );

		add_filter('jpeg_quality', function($arg){return 100;});

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Header', 'gemini-theme' ),
		) );

		register_nav_menus( array(
			'menu-2' => esc_html__( 'Footer', 'gemini-theme' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-logo', array(
			'height'      => 60,
			'width'       => 180,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'gemini_theme_setup' );

function gemini_theme_scripts() {

	wp_enqueue_style( 'gemini-theme-style', get_stylesheet_uri() );

	wp_deregister_script( 'gemini-theme-jquery');
	wp_enqueue_script( 'gemini-theme-jquery', get_template_directory_uri() . '/js/jquery-min.js', array(), null, true);
	wp_enqueue_script( 'gemini-theme-navigation', get_template_directory_uri() . '/js/navigation-min.js', array(), '1', true );
	wp_enqueue_script( 'gemini-theme-customize', get_template_directory_uri() . '/js/customize-min.js', array(), '1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gemini_theme_scripts', 99 );

// Gutenberg support
function gemini_theme_assets() {

	wp_enqueue_style(
		'gemini-theme-blocks-css',
		get_template_directory_uri() . '/blocks.css' ,
		[ 'wp-blocks' ],
		wp_get_theme()->get('Version')
	);

	wp_enqueue_style(
		'gemini-theme-blocks-js',
		get_template_directory_uri() . '/js/blocks-min.js' ,
		[ 'wp-blocks', 'wp-element' ],
		wp_get_theme()->get('Version')
	);
}
add_action( 'enqueue_block_assets', 'gemini_theme_assets' );

add_theme_support( 'align-wide' );

add_theme_support( 'wp-block-styles' );

// Widgets areas support
function gemini_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Home Hero', 'gemini-theme' ),
		'id'            => 'hero-home',
		'description'   => esc_html__( 'Add widgets here.', 'gemini-theme' ),
		'before_widget' => '<article id="hero-home-%1$s" class="hero-home">',
		'after_widget'  => '</article>',
		'before_title'  => '<h1>',
		'after_title'   => '</h1>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'gemini-theme' ),
		'id'            => 'footer-widget',
		'description'   => esc_html__( 'Add widgets here.', 'gemini-theme' ),
		'before_widget' => '<article id="footer-widget-%1$s" class="footer-widget">',
		'after_widget'  => '</article>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'gemini_theme_widgets_init' );

// Remove scripts from head
function move_scripts_from_head_to_footer() {
	remove_action( 'wp_head', 'wp_print_scripts' );
	remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
	remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );

	add_action( 'wp_footer', 'wp_print_scripts', 5);
	add_action( 'wp_footer', 'wp_enqueue_scripts', 5);
	add_action( 'wp_footer', 'wp_print_head_scripts', 5);
}
add_action('wp_enqueue_scripts', 'move_scripts_from_head_to_footer');

function force_jquery_to_footer() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
	wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'force_jquery_to_footer' );

// Remove jQuery from old wp_print_scripts
function remove_jquery_from_wp_print_scripts() {
	wp_deregister_script( 'jquery' );
}
add_action( 'wp_print_scripts', 'remove_jquery_from_wp_print_scripts' );


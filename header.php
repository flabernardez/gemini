<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php

	$thumb      = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$thumb_home = get_the_post_thumbnail_url( 18 );

?>
<header id="masthead" class="site-header" style="
	background-image: linear-gradient(rgba(63, 58, 44, 0.7),rgba(63, 58, 44, 0.7)),
	url('<?php

	if ( is_home() ) {
		echo $thumb_home;
	} else {
		echo $thumb['0'];
	}

	?>');">

	<section class="top-bar">

		<article class="wrapper">

			<section class="site-title">
				<?php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				$url = home_url( '/', 'https' );
				if ( has_custom_logo() ) {
					echo '<a href="'. esc_url( $url ) .'" title="logo inicio"><img src="'. esc_url( $logo[0] ) .'" alt="logo ' . get_bloginfo( 'name' ) . '" title="logo ' . get_bloginfo( 'name' ) . '"></a>';
				} else {
					echo '<a href="'. esc_url( $url ) .'" title="logo inicio">'. get_bloginfo( 'name' ) .'</a>';
				}
				?>
			</section>
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'taurus-theme' ); ?></button>
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</nav><!-- #site-navigation -->

		</article>

	</section><!-- .top-bar -->
	<section class="hero">

		<?php
		if ( is_front_page() ) :

			dynamic_sidebar( 'hero-home' );

		elseif ( is_home() ) : ?>

			<h1> <?php esc_html_e( 'Blog', 'gemini-theme' ) ?></h1>

		<?php
		else : ?>

			<h1><?php the_title(); ?></h1>

		<?php
		endif; ?>

	</section>

</header><!-- #masthead -->


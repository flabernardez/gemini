<?php

get_header(); ?>

	<main id="content" class="site-content">

		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php $thumb_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

                    <header class="post-header" style="
                        background-image: linear-gradient(rgba(63, 58, 44, 0.7),rgba(63, 58, 44, 0.7)),
                        url('<?php echo $thumb_post['0'] ?>');">

                        <a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </header>

                    <section class="post-content">
                        <span class="post-content-meta"><?php the_date();?></span>
                        <p class="post-content-text">
                            <?php the_excerpt(); ?>
                        </p>
					</section>

                </article><!-- #post-<?php the_ID(); ?> -->

			<?php
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</main><!-- #content -->

<?php

get_footer(); ?>
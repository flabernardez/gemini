<?php

get_header(); ?>

    <main id="content" class="site-content">

	    <?php
	    if ( have_posts() ) :

		    /* Start the Loop */
		    while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <header class="entry-header">
                    </header>

                    <section class="entry-content">

		                <?php

		                the_content( sprintf(
			                wp_kses(
			                /* translators: %s: Name of current post. Only visible to screen readers */
				                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'gemini-theme' ),
				                array(
					                'span' => array(
						                'class' => array(),
					                ),
				                )
			                ),
			                get_the_title()
		                ) );

		                wp_link_pages( array(
			                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gemini-theme' ),
			                'after'  => '</div>',
		                ) );
		                ?>

                    </section><!-- .entry-content -->

                </article><!-- #post-<?php the_ID(); ?> -->

		    <?php
            endwhile;

		    the_posts_navigation();

	    else :

		    get_template_part( 'template-parts/content', 'none' );

	    endif; ?>

    </main><!-- #content -->

<?php

get_footer(); ?>